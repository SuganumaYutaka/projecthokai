﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneChanger : MonoBehaviour {
	float fadeLength;
	float fadeTimeCount = 0;
	bool inOrOut = false; //in:false out:true
	public bool InOrOut {
		get { return inOrOut; }
	}
	public bool isFade = false;
	string nextScene;

	Image fadePanel;


	// Use this for initialization
	void Start() {
		fadePanel = GameObject.Find("FadePanel").GetComponent<Image>();
	}

	//開始
	void FadeStart(bool inOrOut /*in:false out:true*/, float fadeLength, string nextScene) {
		if (!isFade) {
			this.inOrOut = inOrOut;
			this.fadeLength = fadeLength;
			this.nextScene = nextScene;
			isFade = true;
		}
	}


	//時間について管理
	void TimerUpdate() {
		if (isFade) {
			//フェードインアウト終了
			if (fadeLength < fadeTimeCount) {
				//フェードアウトならシーン変更
				if (inOrOut == true && nextScene != "") SceneManager.LoadScene(nextScene);
				//終了
				isFade = false;
			}

			fadeTimeCount += Time.deltaTime;
		}
		else {
			fadeTimeCount = 0;
		}
	}

	//貼り付ける画像
	void PanelUpdate() {
		float alpha;

		//in
		if (inOrOut == false) alpha = 1.0f - fadeTimeCount / fadeLength;
		//out
		else alpha = fadeTimeCount / fadeLength;

		if (isFade) fadePanel.color = new Color(fadePanel.color.r, fadePanel.color.g, fadePanel.color.b, alpha);
		else fadePanel.color = new Color(fadePanel.color.r, fadePanel.color.g, fadePanel.color.b, 0);
	}


	//変更を指示
	void Manager() {
		//テスト
		if (SceneManager.GetActiveScene().name == "Tsuruda") {
			if (Input.GetKeyDown(KeyCode.Mouse0)) {
				FadeStart(true, 0.5f, "Tsuruda");
				Debug.Log("hoge");
			}
		}

		//タイトル画面　スコア削除
		if (Input.GetKey(KeyCode.P) && Input.GetKeyDown(KeyCode.Return)) {
			PlayerPrefs.SetInt("rank1", 0);
			PlayerPrefs.SetInt("rank2", 0);
			PlayerPrefs.SetInt("rank3", 0);
			PlayerPrefs.SetInt("rank4", 0);
			PlayerPrefs.SetInt("rank5", 0);
			PlayerPrefs.Save();
		}
	}



	// Update is called once per frame
	void Update() {
		Manager();
		PanelUpdate();

		TimerUpdate();
	}


	public void LoadTitle() {
		FadeStart(true, 0.5f, "Master_Title");
	}

	public void LoadStage(int stageNum) {
		if (stageNum == 1) FadeStart(true, 0.5f, "Master_Stage1");
		else if (stageNum == 2) FadeStart(true, 0.5f, "Master_Stage2");
		else if (stageNum == 3) FadeStart(true, 0.5f, "Master_Stage3");
	}

	public void LoadTutorial() {
		FadeStart(true, 1.0f, "Tutorial");
	}
}
