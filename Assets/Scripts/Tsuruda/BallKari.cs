﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallKari : MonoBehaviour {
	public Rigidbody rb;
	public Vector3 vel;

	float time = 0;


	// Use this for initialization
	void Start() {

	}

	// Update is called once per frame
	void Update() {
		if (rb.isKinematic && time > 1f) {
			rb.isKinematic = false;
			rb.velocity = vel;
		}

		time += Time.deltaTime;
	}
}
