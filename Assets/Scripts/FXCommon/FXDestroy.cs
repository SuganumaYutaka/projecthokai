﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//エフェクトの再生が終了したらエフェクトのGameObjectを破壊します
public class FXDestroy : MonoBehaviour {
	float time = 0;
	public float lifeTime; //エフェクトのGameObjectの寿命。多めに取る分には大丈夫。
	public ParticleSystem ps;


	// Use this for initialization
	void Start() {

	}

	// Update is called once per frame
	void Update() {
		if (time < lifeTime) {
			if (ps.isPlaying) {
				time += Time.deltaTime;
			}
		}
		else {
			Destroy(gameObject);
		}
	}
}
