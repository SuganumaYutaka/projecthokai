﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXTester : MonoBehaviour {
	float time = 0;

	public FXStarter fxThrowImpactStarter;
	bool fxThrowImpactPlayFlag;

	public FXStarter fxThrowSmokeStarter;
	bool fxThrowSmokePlayFlag;
	public Vector3 fxThrowSmokePos;


	// Use this for initialization
	void Start() {

	}

	// Update is called once per frame
	void Update() {
		fxThrowImpactPlayFlag = 1.0f < time && time < 1.0f + 0.1f;
		fxThrowSmokePlayFlag = 0.8f < time && time < 0.8f + 0.1f;

		if (fxThrowImpactPlayFlag) {
			fxThrowImpactStarter.StartFX();
		}

		if (fxThrowSmokePlayFlag) {
			fxThrowSmokeStarter.StartFX(fxThrowSmokePos, false);
		}

		time += Time.deltaTime;
	}
}
