﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//エフェクトを再生します。
public class FXStarter : MonoBehaviour {
	public ParticleSystem ps;

	public void StartFX() {
		if(!ps.isPlaying) ps.Play(true);
	}

	public void StartFX(Vector3 pos, bool isLocal) {
		if (!ps.isPlaying) {
			if(isLocal) ps.transform.localPosition = pos;
			else ps.transform.position = pos;

			ps.Play(true);
		}
	}
}
