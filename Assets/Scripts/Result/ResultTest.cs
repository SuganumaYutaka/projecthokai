﻿using UnityEngine;
using System.Collections;

public class ResultTest : MonoBehaviour
{
    [SerializeField] int Score;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ResultSpawn()
    {
        ResultManager.instance.SetScore(Score);
    }
}
