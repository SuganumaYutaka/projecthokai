﻿using UnityEngine;
using System.Collections;

public class ResultManager : MonoBehaviour
{
    public static ResultManager instance;
    private void Awake()
    {
        instance = this;
    }

    private int Score = 0;
    [SerializeField] ResultScore resultScore;
    [SerializeField] Ranking ranking;
    [SerializeField] GameObject resultUIObject;

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        resultScore.SetScore(Score);
    }

    public void SetScore( int score)
    {
        Score = score;
        resultUIObject.SetActive(true);
        resultScore.SetScore(Score);
        ranking.SetNewRank(Score);
    }
}
