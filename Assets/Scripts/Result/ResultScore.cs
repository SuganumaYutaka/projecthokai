﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ResultScore : MonoBehaviour
{

    [SerializeField] int Score;
    [SerializeField] Text text;

    // Use this for initialization
    void Start()
    {
        Score = 0;
        text.text = Score.ToString();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetScore( int score)
    {
        Score = score;
        text.text = Score.ToString();
    }
}
