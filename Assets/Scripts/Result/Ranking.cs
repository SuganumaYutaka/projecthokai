﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ranking : MonoBehaviour {

    const string RANK_1 = "rank1";
    const string RANK_2 = "rank2";
    const string RANK_3 = "rank3";
    const string RANK_4 = "rank4";
    const string RANK_5 = "rank5";

    private List<int> Ranks = new List<int>();

    [SerializeField] Text[] Texts;

    // Use this for initialization
    void Awake()
    {
        //読み込み
        for (int i = 0; i < 5; i++)
        {
            Ranks.Add(0);
        }
        Load();

        for (int i = 0; i < 5; i++)
        {
            Texts[i].text = Ranks[i].ToString();
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    void Load()
    {
        Ranks[0] = PlayerPrefs.GetInt( RANK_1, 0);
        Ranks[1] = PlayerPrefs.GetInt( RANK_2, 0);
        Ranks[2] = PlayerPrefs.GetInt( RANK_3, 0);
        Ranks[3] = PlayerPrefs.GetInt( RANK_4, 0);
        Ranks[4] = PlayerPrefs.GetInt( RANK_5, 0);
    }

    void Save()
    {
        PlayerPrefs.SetInt(RANK_1, Ranks[0]);
        PlayerPrefs.SetInt(RANK_2, Ranks[1]);
        PlayerPrefs.SetInt(RANK_3, Ranks[2]);
        PlayerPrefs.SetInt(RANK_4, Ranks[3]);
        PlayerPrefs.SetInt(RANK_5, Ranks[4]);
        PlayerPrefs.Save();
    }

    public void SetNewRank( int score)
    {
        Ranks.Add(score);
        Ranks.Sort((a, b) => b - a);
        Save();

        for (int i = 0; i < 5; i++)
        {
            Texts[i].text = Ranks[i].ToString();
        }
    }
}
