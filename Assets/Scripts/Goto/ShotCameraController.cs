﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotCameraController : MonoBehaviour {
	[SerializeField]
	private Camera TargetCam;
	[SerializeField]
	private MeshRenderer[] XAxis;
	[SerializeField]
	private MeshRenderer[] YAxis;

	[SerializeField]
	private GameObject XAxisCameraPos;
	[SerializeField]
	private GameObject YAxisCameraPos;
	[SerializeField]
	private GameObject PowerCameraPos;
	[SerializeField]
	private GameObject OverViewPos;
	[SerializeField]
	private GameObject CameraLookPos;
	[SerializeField]
	private GameObject OverViewLookPos;

	[SerializeField]
	private Transform HitStopCameraPos;

	// Use this for initialization
	void Start () {
		ChangeYAxisCam ();
	}

	public void ChangeXAxisCam(){
		foreach (MeshRenderer mesh in YAxis) {
			mesh.enabled = false;
		}
		foreach (MeshRenderer mesh in XAxis) {
			mesh.enabled = true;
		}
		TargetCam.transform.position = XAxisCameraPos.transform.position;
		TargetCam.transform.LookAt (CameraLookPos.transform.position);
	}

	public void ChangeYAxisCam(){
		foreach (MeshRenderer mesh in XAxis) {
			mesh.enabled = false;
		}
		foreach(MeshRenderer mesh in YAxis){
			mesh.enabled = true;
		}
		TargetCam.transform.position = YAxisCameraPos.transform.position;
		TargetCam.transform.LookAt (CameraLookPos.transform.position);
	}

	public void ChangePowerCam(){
		foreach (MeshRenderer mesh in XAxis) {
			mesh.enabled = false;
		}
		foreach (MeshRenderer mesh in YAxis) {
			mesh.enabled = true;
		}
		TargetCam.transform.position = PowerCameraPos.transform.position;
		TargetCam.transform.LookAt (CameraLookPos.transform.position);
	}

	public void ChangeOverViewCam(){
		foreach (MeshRenderer mesh in YAxis) {
			mesh.enabled = false;
		}
		foreach (MeshRenderer mesh in XAxis) {
			mesh.enabled = false;
		}
		TargetCam.transform.position = OverViewPos.transform.position;
		TargetCam.transform.LookAt (OverViewLookPos.transform.position);
	}

	public void ChangeHitStopCameraPos(){		
		foreach (MeshRenderer mesh in XAxis) {
			mesh.enabled = false;
		}
		foreach (MeshRenderer mesh in YAxis) {
			mesh.enabled = false;
		}

		TargetCam.transform.position = HitStopCameraPos.position;
		TargetCam.transform.LookAt (CameraLookPos.transform.position);
	}

	// Update is called once per frame
	void Update () {

	}


}
