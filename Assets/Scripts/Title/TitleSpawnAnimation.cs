﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleSpawnAnimation : MonoBehaviour {

    [SerializeField] float AnimationTime = 1.0f;
    [SerializeField] float FirstScaleRate = 0.0f;

    private RectTransform rectTransform;
    public Vector2 Size;
    private float DeltaScaleUp = 0.0f;
    private float ScaleRate = 0.0f;

	// Use this for initialization
	void Start () {
        //1秒あたりに拡大する量算出
        ScaleRate = FirstScaleRate;
        DeltaScaleUp = (1.0f - FirstScaleRate) / AnimationTime;

        //初期スケール設定
        rectTransform = GetComponent<RectTransform>();
        Size = rectTransform.sizeDelta;
        rectTransform.sizeDelta *= ScaleRate;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (ScaleRate < 1.0f)
        {
            ScaleRate = Mathf.Min(1.0f, ScaleRate + DeltaScaleUp * Time.deltaTime);
            rectTransform.sizeDelta += Size * DeltaScaleUp * Time.deltaTime;
        }
	}
}
