﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour {
	public float time = 0;
	public float timeLimit;
	public bool isThrow = false;
	public bool isExe = false;
	public BlockGen blockGen;
	public ResultManager rm;

	// Use this for initialization
	void Start() {

	}

	// Update is called once per frame
	void Update() {
		if (isThrow) {
			time += Time.deltaTime;
			if (!isExe) {
				if (time > timeLimit) {
					int point = blockGen.calcPoint();
					rm.SetScore(point);
					isExe = true;
				}
			}
		}
	}

	public void setThrow() {
		isThrow = true;

	}
}
