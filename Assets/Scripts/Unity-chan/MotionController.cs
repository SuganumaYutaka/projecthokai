﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotionController : MonoBehaviour
{

    [SerializeField] Animator animator;
    [SerializeField]  float AttackMoveValue = 1.0f;
    [SerializeField]  float AttackMoveTime = 0.5f;

	public FXStarter fxThrowImpactStarter;
	public FXStarter fxThrowSmokeStarter;


	// Use this for initialization
	void Start()
    {
        animator.SetInteger("Phase", 0);
    }

    // Update is called once per frame
    void Update()
    {
        //攻撃時の移動
        if (animator.GetInteger("Phase") == 2 && AttackMoveTime > 0.0f)
        {
            AttackMoveTime -= Time.deltaTime;
            transform.Translate(0.0f, 0.0f, AttackMoveValue);

			if(AttackMoveTime <= 0.0f) {
				fxThrowImpactStarter.StartFX();
				fxThrowSmokeStarter.StartFX();
			}
        }
    }

    public void SetKamaeMotion()
    {
        animator.SetInteger("Phase", 1);
        animator.speed = 0.3f;
    }

    public void SetAttackMotion()
    {
        animator.SetInteger("Phase", 2);
        animator.speed = 1.0f;
    }

    public void SetBackMotion()
    {
        animator.SetInteger("Phase", 3);
        animator.speed = 0.5f;
    }
}
