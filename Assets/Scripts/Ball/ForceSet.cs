﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ForceSet : MonoBehaviour {
	[SerializeField] RectTransform PowerBlueGage;
    [SerializeField] Shotdata shotdata;
    [SerializeField] Shot shot;
	[SerializeField] 
	[Header("最小パワー")]
	float MinPower;		// 最低パワー
	[SerializeField] 
	[Header("最大パワー")]
	float MaxPower;		// 最大パワー
	[SerializeField] 
	[Header("minからmaxになるまでの時間[sec]")]
	float PowerSeekTime;	// minからmaxになる時間[sec]
	//float Forcevalue = 0.0f;
    //float deltaValue = 1000.0f;

	[Header("デバッグ用表示　変更禁止")]
	[SerializeField]
	float Forcevalue = 0;
	[SerializeField]
	float ImageMaxHight;
	[SerializeField]
	float TimeBuffer;
	[SerializeField]
	bool SeekSgn;

	void Start(){
		SeekSgn = false;
		ImageMaxHight = PowerBlueGage.sizeDelta.y;
	}

    void Update()
	{
		if (SeekSgn) {
			// 減る
			if (TimeBuffer <= PowerSeekTime) {
				Forcevalue = Mathf.Lerp (MaxPower, MinPower, TimeBuffer / PowerSeekTime);
				PowerBlueGage.sizeDelta = new Vector2(PowerBlueGage.sizeDelta.x,Mathf.Lerp (ImageMaxHight,0,TimeBuffer / PowerSeekTime));
				TimeBuffer += Time.deltaTime;
			} else {
				TimeBuffer = 0;
				SeekSgn = !SeekSgn;
			}
		} else {
			if (TimeBuffer <= PowerSeekTime) {
				Forcevalue = Mathf.Lerp (MinPower,MaxPower, TimeBuffer / PowerSeekTime);
				PowerBlueGage.sizeDelta = new Vector2(PowerBlueGage.sizeDelta.x,Mathf.Lerp (0,ImageMaxHight,TimeBuffer / PowerSeekTime));
				TimeBuffer += Time.deltaTime;
			} else {
				TimeBuffer = 0;
				SeekSgn = !SeekSgn;
			}
		}
		//入力
		if (Input.GetKeyDown(KeyCode.Space))
		{
			shotdata.ForceValue = Forcevalue;
			shot.enabled = true;
			Destroy (this);
		}
	}
}
