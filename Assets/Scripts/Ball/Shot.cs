﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot : MonoBehaviour
{
	[SerializeField] Shotdata shotdata;
	[SerializeField] ShotCameraController ShotCont;
	[SerializeField] float HitStopTime;
	[SerializeField] Camera HitStopCamera;
	[SerializeField] Transform LookTarget;
	[SerializeField] Transform HitStopCameraPosRoot;
	[SerializeField] Transform HitStopCameraPos;
	[SerializeField] float HitStopStartAngle;
	[SerializeField] float HitStopEndAngle;
	[SerializeField]
	Timer timer;



	[Header("デバッグ用表示　変更禁止")]
	[SerializeField]
	private float TimeBuff;



	void Start(){
		ShotCont.ChangeHitStopCameraPos ();
		shotdata.motion.SetAttackMotion();
	}

    private void Update()
    {
		if (TimeBuff >= HitStopTime) {
			transform.rotation = Quaternion.Euler (shotdata.RotationValueX, shotdata.RotationValueY, 0.0f);

			Rigidbody rigidbody = GetComponent<Rigidbody> ();
			rigidbody.isKinematic = false;
			rigidbody.AddForce (transform.forward * shotdata.ForceValue, ForceMode.Impulse);

			timer.setThrow();
			shotdata.motion.SetBackMotion();


			Destroy(this);
		} else {
			HitStopCameraPosRoot.rotation = Quaternion.Euler (
				HitStopCameraPosRoot.rotation.eulerAngles.x,
				Mathf.Lerp (HitStopStartAngle, HitStopEndAngle, TimeBuff / HitStopTime),
				HitStopCameraPosRoot.rotation.eulerAngles.z
			);
			HitStopCamera.transform.position = HitStopCameraPos.transform.position;
			HitStopCamera.transform.LookAt (LookTarget);

			TimeBuff += Time.deltaTime;
		}
    }
}
