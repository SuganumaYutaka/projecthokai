﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {

    [SerializeField] GameObject ExplosionObject;

	public FXStarter fxExplosionStarter;


	// Use this for initialization
	void Start ()
    {
        ExplosionObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}


    private void OnCollisionEnter(Collision collision)
    {
        if( collision.collider.tag == "Block")
        {
            //爆発
            ExplosionObject.SetActive(true);
            ExplosionObject.transform.position = transform.position;
            ExplosionObject.transform.parent = transform.parent;

			fxExplosionStarter.StartFX(transform.position, false);

			Destroy(gameObject);
        }
    }
}
