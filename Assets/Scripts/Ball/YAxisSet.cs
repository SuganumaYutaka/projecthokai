﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YAxisSet : MonoBehaviour
{
    [SerializeField] Shotdata shotdata;
    [SerializeField] XAXISSet xAXISSet; 
	[SerializeField] ShotCameraController ShotCont;
    float RotationValueY;
    float deltaValue = 45.0f;

    int mode = 0;

	void Start(){
		ShotCont.ChangeYAxisCam ();
	}

    void Update()
    {
        if (mode == 0)
        {
            RotationValueY += deltaValue * Time.deltaTime;
            if (RotationValueY > 45)
            {
                mode = 1;
            }

        }
        else
        {
            RotationValueY -= deltaValue * Time.deltaTime;
            if (RotationValueY < -45)
            {
                mode = 0;
            }
        }
        //入力
        if (Input.GetKeyDown(KeyCode.Space))
        {
            shotdata.RotationValueY = RotationValueY;
            xAXISSet.enabled = true;
            Destroy(this);
			ShotCont.ChangeXAxisCam ();
        }
        transform.rotation = Quaternion.Euler(shotdata.RotationValueX, RotationValueY, 0.0f);
    }

}




