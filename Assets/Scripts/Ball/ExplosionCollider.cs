﻿using UnityEngine;
using System.Collections;

public class ExplosionCollider : MonoBehaviour
{
    [SerializeField] float LifeTime = 0.5f;
    public float MaxForceValue = 3.0f;
    public float MaxDistance = 3.0f;

    // Use this for initialization
    void Start()
    {
        MaxDistance = transform.localScale.x;
    }

    // Update is called once per frame
    void Update()
    {
        LifeTime -= Time.deltaTime;

        if( LifeTime <= 0.0f)
        {
            Destroy(gameObject);
            return;
        }
    }
}
