﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XAXISSet : MonoBehaviour {
	[SerializeField] ShotCameraController ShotCam;
    [SerializeField] Shotdata shotdata;
    [SerializeField] ForceSet ForceSet;
	[SerializeField] float MaxAngle = 45f;
	[SerializeField] float MinAngle = -45f;
    float RotationValueX;
    float deltaValue = 45.0f;

    int mode = 0;

    void Update()
    {
        if (mode == 0)
        {
            RotationValueX += deltaValue * Time.deltaTime;
			if (RotationValueX > MaxAngle)
            {
                mode = 1;
            }

        }
        else
        {
            RotationValueX -= deltaValue * Time.deltaTime;
			if (RotationValueX < MinAngle)
            {
                mode = 0;
            }
        }
        //入力
        if (Input.GetKeyDown(KeyCode.Space))
        {
            shotdata.RotationValueX = RotationValueX;
            ForceSet.enabled = true;
			ShotCam.ChangePowerCam ();
			shotdata.motion.SetKamaeMotion();
            Destroy(this);
        }
        transform.rotation = Quaternion.Euler(RotationValueX, shotdata.RotationValueY, 0.0f);
    }

}
