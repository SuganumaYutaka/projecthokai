﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowImpactTrail : MonoBehaviour {
	Rigidbody thisRb;
	float time = 0;
	public float lifeTime;
	public float vel;

	// Use this for initialization
	void Start() {
		thisRb = GetComponent<Rigidbody>();
		thisRb.velocity = new Vector3(vel, 0, 0);
	}

	// Update is called once per frame
	void Update() {
		if (time > lifeTime) {
			thisRb.velocity = new Vector3(0, 0, 0);
		}
		else {
			time += Time.deltaTime;
		}
	}
}
