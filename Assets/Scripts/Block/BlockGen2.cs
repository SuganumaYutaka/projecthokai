﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockGen2 : MonoBehaviour {
    public GameObject block, bonus;
    public int dan, width;
    public float bonusPoint1, bonusPoint2;
    private float genHeight, blockX, blockY, blockZ;
    private int point;
    private List<Vector3> start;
    private List<int> bn, bonusN;
    private RigSphere ball;
    private ResultManager rm;
    private bool tf, bn1, bn2, bn3;

    void Start() {
        blockX = block.transform.localScale.x;
        blockY = block.transform.localScale.y;
        blockZ = block.transform.localScale.z;
        genHeight = blockZ / 2;

        ball = GameObject.Find("Ball").GetComponent<RigSphere>();
        rm = GameObject.Find("ResultCanvas").GetComponent<ResultManager>();

        int n = 0;
        bn = new List<int>();
        bonusN = new List<int>();

        bn2 = false;
        for (int k = 0; k < dan; k++) {
            float y = -blockY * (width / 2);
            if (k % 2 == 1) {
                bn3 = true;
                //y = -blockY * (width / 2);
            } else {
                bn3 = false;
                //y = -blockY * (width / 2) - (blockY / 2);
            }

            for (int j = 0; j < width; j++) {
                float x = -blockX * (width / 2);
                //if (k % 2 == 1) x = -blockX * (width / 2);
                //else x = -blockX * (width / 2) - (blockX / 2);

                if (j == 0 || j == width - 1) bn1 = true;
                else bn1 = false;

                for (int i = 0; i < width; i++) {
                    if (i == 0 || i == width - 1) bn2 = true;
                    else bn2 = false;
                    GameObject obj = null;
                    if (bn3 && bn1 && bn2) {
                        obj = Instantiate(bonus, new Vector3(x, genHeight + (blockZ * k), y), Quaternion.identity);
                        bonusN.Add(n);
                    } else obj = Instantiate(block, new Vector3(x, genHeight + (blockZ * k), y), Quaternion.identity);
                    obj.transform.parent = transform;
                    x += blockX;
                    n++;
                }
                y += blockY;
            }
            bn3 = false;
        }

        Transform[] objList = GetComponentsInChildren<Transform>();
        start = new List<Vector3>();
        foreach (Transform obj in objList) {
            float x = (float)Math.Round(obj.transform.position.x, 1, MidpointRounding.AwayFromZero);
            float y = (float)Math.Round(obj.transform.position.y, 1, MidpointRounding.AwayFromZero);
            float z = (float)Math.Round(obj.transform.position.z, 1, MidpointRounding.AwayFromZero);
            start.Add(new Vector3(x, y, z));
        }

        point = 0;
        tf = true;
    }

    void Update() {
        if (ball.getEnd()) {
            if (tf) {
                Rigidbody[] objList = GetComponentsInChildren<Rigidbody>();
                bool a = true;
                foreach (Rigidbody obj in objList) {
                    if (obj.velocity.magnitude >= 2f) a = false;
                }
                if (a) tf = false;
            } else {

                int n = 0;
                Transform[] objList = GetComponentsInChildren<Transform>();
                foreach (Transform obj in objList) {
                    float x = (float)Math.Round(obj.transform.position.x, 1, MidpointRounding.AwayFromZero);
                    float y = (float)Math.Round(obj.transform.position.y, 1, MidpointRounding.AwayFromZero);
                    float z = (float)Math.Round(obj.transform.position.z, 1, MidpointRounding.AwayFromZero);
                    Vector3 now = new Vector3(x, y, z);

                    if (!now.Equals(start[n])) {
                        float length = now.magnitude;
                        float kyori = 1f;
                        if (length >= bonusPoint2) kyori = 2f;
                        else if (length >= bonusPoint1) kyori = 1.5f;

                        if (bonusN.Contains(n)) kyori *= 2;

                        point += (int)Math.Round(length * kyori, 0, MidpointRounding.AwayFromZero);
                    }
                    n++;
                }

                rm.SetScore(point);
                Destroy(this);
            }
        }
    }
}
