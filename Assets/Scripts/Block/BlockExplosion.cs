﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockExplosion : MonoBehaviour
{
    Rigidbody rigidbody;
    
    // Use this for initialization
    void Start () {
        rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        var explosion = collision.collider.GetComponent<ExplosionCollider>();
		if (explosion == null) {
			return;
		}
		float MaxForceValue = explosion.MaxForceValue;
        float MaxDistance = explosion.MaxDistance;
        Vector3 vector = transform.position - collision.collider.transform.position;
        float distance = vector.magnitude;


        float force = (MaxDistance - distance) / MaxDistance * MaxForceValue;
        rigidbody.AddForce(vector.normalized * force);
    }
}
