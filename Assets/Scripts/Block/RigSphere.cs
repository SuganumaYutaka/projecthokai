﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigSphere : MonoBehaviour {
	public GameObject walls;
	public float speed;
	private Rigidbody rb;
	private bool ins, end;

	bool isFirst = true;


	void Start() {
		rb = GetComponent<Rigidbody>();
		rb.velocity = new Vector3(speed, speed, 0);
		ins = true;
		end = false;
	}

	void Update() {
		if ((transform.position.x > -40 && transform.position.x < 40) && (transform.position.z > -40 && transform.position.z < 40) && ins) {
			ins = false;
			Instantiate(walls);
		}

		if (transform.position.y <= -15) {
			end = true;
			ins = false;
			print("soto");
		}

		if (!isFirst) {
			if (rb.velocity.magnitude <= 2.0f) {
				rb.velocity = Vector3.zero;
				rb.isKinematic = true;
				end = true;
			}
		}
		else {
			if (rb.velocity.magnitude > 2.0f) {
				isFirst = false;
			}
		}
	}


	private void OnCollisionEnter(Collision other) {
		if (other.gameObject.CompareTag("Floor")) {
			rb.velocity *= 0.9f;
		}
	}

	public bool getEnd() {
		return end;
	}
}
